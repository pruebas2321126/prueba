import React from 'react';
import TodoList from '../components/TodoList';
import styles from '../styles/Home.module.scss'

const HomePage = () => {
  return (
    <div className="app">
      <h1 className={styles.title}>Lista de Tareas</h1>
      <TodoList />
    </div>
  );
};

export default HomePage;
