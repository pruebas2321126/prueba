// src/components/TodoList.js
import React, { useEffect, useState } from 'react';
import TodoItem from './TodoItem';
import styles from '../styles/TodoList.module.scss'

const TodoList = () => {
  const [todos, setTodos] = useState([]);
  const [actualTaskAdd, setActualTaskAdd] = useState('')
  
  // Agregar una nueva tarea
  const addTodo = (title) => {
    const newTodo = {
      id: Date.now(),
      title,
      completed: false,
    };
    setTodos([...todos, newTodo]);
    setActualTaskAdd('')
  };

  // Marcar una tarea como completada
  const toggleComplete = (id) => {
    setTodos((prevTodos) =>
      prevTodos.map((todo) =>
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      )
    );
  };

  // Eliminar una tarea
  const deleteTodo = (id) => {
    setTodos((prevTodos) => prevTodos.filter((todo) => todo.id !== id));
  };

  return (
    <div className={styles.containerList}>
      <div className={styles.addTaskList}>
        <label>Ingresa tu tarea</label>
        <input className={styles.inputTask} type="text" onChange={(e) => setActualTaskAdd(e.target.value)} value={actualTaskAdd}/>
        <button disabled={actualTaskAdd === ''} className={styles.buttonAddTask} onClick={() => addTodo(actualTaskAdd)}>Agregar Tarea</button>
      </div>
      {
        todos.length > 0 &&
        <div className={styles.todoList}>
          {todos.map((todo) => (
            <TodoItem
              key={todo.id}
              todo={todo}
              toggleComplete={toggleComplete}
              deleteTodo={deleteTodo}
            />
          ))}
        </div>
      }
    </div>
  );
};

export default TodoList;
