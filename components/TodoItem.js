import React from 'react';
import styles from '../styles/TodoItem.module.scss'

const TodoItem = ({ todo, toggleComplete, deleteTodo }) => {
  return (
    <div className={`${styles.todoItem} ${todo.completed ? styles.todoItemCompleted : ''}`}>
      <label onChange={() => toggleComplete(todo.id)}>
        <input type="checkbox" value={todo.completed} />{todo.completed ? 'Marcar como Incompleta' : 'Completar Tarea'}
      </label>
      <span>{todo.title}</span>
      <button className={styles.buttonList} onClick={() => deleteTodo(todo.id)}>Eliminar</button>
    </div>
  );
};

export default TodoItem;
